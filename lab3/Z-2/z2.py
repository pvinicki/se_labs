# ups: ispisi sve brojeve od 1 do 100 (uključujući i 1 i 100), svaki broj 
# u novi red, ali umjesto broja koji je djeljiv sa 3 ispiši "ups", a umjesto 
# broja koji sadrzi broj 3 ispisi hops
#
# Ukoliko broj i sadrži 3 i djeljiv je sa 3 (npr. broj 30), ispiši "upshops"

for i in range(1, 101):
    if (i % 3 == 0 and (i / 10 == 3 or i % 10 == 3)):
        print(str(i) + " upshops")

    elif i % 3 == 0:
        print(str(i) + " ups")
    
    elif i / 10 == 3 or i % 10 == 3:
        print(str(i) + " hops")