bdays = {}
bdays['Albert Einstein'] = '14. ožujka 1879'
bdays['Benjamin Franklin'] = '17. siječnja 1706.'
bdays['Ada Lovelace'] = '10. prosinca 1815.'


print("Welcome to the birthday dictionary. We know the birthdays of:")

for person in bdays.keys():
    print(person)

search = input("Who's birthday do you want to look up?")

print(bdays[search])
