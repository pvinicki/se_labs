print("imagine a number between 0 and 100, the program will guess it!")
max = 100
min = 0

while True:
    guess = (max+min)/2

    print("is %d your number? [y/n]" % (guess))
    action = input()

    if action == 'y':
        print("That was easy!")
        break
    elif action == 'n':
        print("Is your number higher(h) or lower(l)?")
        action = input()

        if action == 'h':
            min = guess
        elif action == 'l':
            max = guess
    
