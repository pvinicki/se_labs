string = input("Enter a word")

string_reversed = string[::-1]

if string == string_reversed:
    print("The word is a palindrome")
else:
    print("The word is not a palindrome")
