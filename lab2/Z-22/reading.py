with open('nameslist.txt', 'r') as file:

    names = {}

    while (file.readline() != ''):
        line = file.readline().strip()

        if line in names.keys():
           names[line] += 1

        else:
            names[line] = 1

    print (names)
