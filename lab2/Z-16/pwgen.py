import random

pwstring = "abcdefghijklmnopqrstuvwxyz0123456789!$&%/()"
pwlist = [char for char in pwstring]
counter = 10
password = []
while counter > 0:
    randchar = pwlist[random.randint(0, (len(pwlist) - 1))]
    password.append(randchar)
    counter = counter - 1

password = ''.join(password)
print (password)

