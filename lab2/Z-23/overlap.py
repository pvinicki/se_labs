primes = []
happynum = []

with open('primenumbers.txt', 'r') as file:
    line = file.readline().strip()

    while line:
        primes.append(int(line))
        line = file.readline().strip()

with open('happynumbers.txt', 'r') as file:
    line = file.readline().strip()

    while line:
        happynum.append(int(line))
        line = file.readline().strip()

for prime in primes:
    for happy in happynum:
        if prime == happy:
            print(prime)
