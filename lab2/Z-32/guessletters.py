word = 'EVAPORATE'
hidden_word = '_ _ _ _ _ _ _ _ _'
guessed_letters = []

def check(letter):
    if letter in word:
        return True
    else:
        return False

def updateHiddenWord(letter, hidden_word):
    for i in range(0, len(word)):
            if word[i] == letter:
                hidden_word_list = hidden_word.split(' ') 
                hidden_word_list[i] = letter
                hidden_word = ' '.join(hidden_word_list)

    return hidden_word
    

print("Welcome to hangman")

while ('_' in hidden_word) :
    guess = input("Guess a letter:")
    if check(guess):
        print("Correct")
        hidden_word = updateHiddenWord(guess, hidden_word)
        print(hidden_word)
    else:
        print("incorrect")

print("Congratulations, you have won !")
    
    
    
