import random
counter = 0

with open('sowpods.txt','r') as f:
    
    while (f.readline() != ""):
        counter += 1

randword = random.randint(0, counter)

with open('sowpods.txt','r') as f:
    
    while(randword > 0):
        line = f.readline()
        randword -= 1


word = line
hidden_word = ""

for i in range(0, len(word)-1):
    hidden_word += '_ '

hidden_word = hidden_word.rstrip()
print(hidden_word)

guessed_letters = []

def check(letter):
    if letter in word:
        return True
    else:
        return False

def updateHiddenWord(letter, hidden_word):
    for i in range(0, len(word)):
            if word[i] == letter:
                hidden_word_list = hidden_word.split(' ') 
                hidden_word_list[i] = letter
                hidden_word = ' '.join(hidden_word_list)

    return hidden_word
    

print("Welcome to hangman")

while ('_' in hidden_word) :
    guess = input("Guess a letter:")
    if check(guess):
        print("Correct")
        hidden_word = updateHiddenWord(guess, hidden_word)
        print(hidden_word)
    else:
        print("incorrect")

print("Congratulations, you have won !")
    
    
    
