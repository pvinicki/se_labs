def generate_list():
    amount = int(input("How many elements?"))
    lst = []
    while(amount > 0):
        num = int(input("Enter a number"))
        lst.append(num)
        amount -= 1

    return lst

lst = generate_list()

unique = {s for s in lst}

print(lst)
print(unique)
