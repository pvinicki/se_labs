game = [[2, 2, 0],
	[2, 1, 0],
	[2, 1, 1]]
condition = 0

def checkrow(x):
    condition = 0
    
    for i in range(0,3):
        if game[i][0] == x:
            condition += 1

    if condition == 2:
        return True
    else:
        return False

def checkcol(x):
    condition = 0
    
    for i in range(0,3):
        if game[0][i] == x:
            condition += 1

    if condition == 2:
        return True
    else:
        return False

def checkprmdiag(x):
    condition = 0
    
    for i in range(0,3):
        if game[i][i] == x:
            condition += 1

    if condition == 2:
        return True
    else:
        return False

def checksecdiag(x):
    condition = 0
    
    for i in range(3,0):
        if game[i][i] == x:
            condition += 1

    if condition == 2:
        return True
    else:
        return False

for i in range(0,3):
    if game[0][i] == 1 or game[0][i] == 2:
        value = game[0][i]
        
        if(checkrow(value)):
            print("player %d wins" % (value))
            break

        elif(checkcol(value)):
            print("player %d wins" % (value))
            break

        elif i == 0:
            if(checkprmdiag(value)):
                print("player %d wins" % (value))
                break
        elif i == 2:
            if(checksecdiag(value)):
                print("player %d wins" % (value))
                break
